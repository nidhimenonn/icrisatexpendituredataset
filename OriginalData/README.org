This directory is meant to hold the original, most 'primitive' files
available.  Some notes on management:

  - These files may be obtained as a =zip= or other sort of compressed
    archive.  It's these archives which should be added to the
    underlying repository.
  - In the case of an archive, you may wish to uncompress or extract
    the archive contents in this directory.  That is fine, but you
    should only add the archive to the repository, not the files
    extracted from it.
  - The data files in this directory should be regarded as being
    *static*; they should never change.
  - If possible, these static files should be added to the =git=
    repository as =lfs= files; see, e.g.,
    https://github.com/git-lfs/git-lfs/wiki/Tutorial.
    #+begin_src sh
    
    #+end_src
  - Any cleaning or fixing should be implemented by writing code which
    takes the files in this directory as /input/, and (perhaps) writes
    the result to a file in the directory =../var=.
